<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Users extends REST_Controller  {

	function __construct($config = 'rest') {
        parent::__construct($config);
    }

	function index_get() {
        $id = $this->get('client_id');
        if ($id == '') {
            $data = $this->db->get('ip_clients')->result();
        } else {
            $this->db->where('client_id', $id);
            $data = $this->db->get('ip_clients')->result();
        }
        $this->response($data, 200);
    }

    function index_post() {
        $data = array(
            'client_date_created'    => date('Y-m-d H:m:s'),
            'client_date_modified'  => $this->post('client_date_modified'),
            'client_name' => $this->post('client_name'),
            'client_address_1' => $this->post('client_address_1'),
            'client_address_2' => $this->post('client_address_2'),
            'client_city' => $this->post('client_city'),
            'client_state' => $this->post('client_state'),
            'client_zip' => $this->post('client_zip'),
            'client_country' => $this->post('client_country'),
            'client_phone' => $this->post('client_phone'),
            'client_fax' => $this->post('client_fax'),
            'client_mobile' => $this->post('client_mobile'),
            'client_email' => $this->post('client_email'),
            'client_web' => $this->post('client_web'),
            'client_vat_id' => $this->post('client_vat_id'),
            'client_tax_code' => $this->post('client_tax_code'),
            'client_language' => $this->post('client_language'),
            'client_active' => $this->post('client_active'),
            'client_surname' => $this->post('client_surname'),
            'client_avs' => $this->post('client_avs'),
            'client_insurednumber' => $this->post('client_insurednumber'),
            'client_veka' => $this->post('client_veka'),
            'client_birthdate' => $this->post('client_birthdate'),
            'client_gender' => $this->post('client_gender'),
        );
        $insert = $this->db->insert('ip_clients', $data);
        if ($insert) {
            $this->response($data, 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_put() {
        $data = array(
            'client_date_created'    => date('Y-m-d H:m:s'),
            'client_date_modified'  => $this->post('client_date_modified'),
            'client_name' => $this->post('client_name'),
            'client_address_1' => $this->post('client_address_1'),
            'client_address_2' => $this->post('client_address_2'),
            'client_city' => $this->post('client_city'),
            'client_state' => $this->post('client_state'),
            'client_zip' => $this->post('client_zip'),
            'client_country' => $this->post('client_country'),
            'client_phone' => $this->post('client_phone'),
            'client_fax' => $this->post('client_fax'),
            'client_mobile' => $this->post('client_mobile'),
            'client_email' => $this->post('client_email'),
            'client_web' => $this->post('client_web'),
            'client_vat_id' => $this->post('client_vat_id'),
            'client_tax_code' => $this->post('client_tax_code'),
            'client_language' => $this->post('client_language'),
            'client_active' => $this->post('client_active'),
            'client_surname' => $this->post('client_surname'),
            'client_avs' => $this->post('client_avs'),
            'client_insurednumber' => $this->post('client_insurednumber'),
            'client_veka' => $this->post('client_veka'),
            'client_birthdate' => $this->post('client_birthdate'),
            'client_gender' => $this->post('client_gender'),
        );

        $this->db->where('client_id', $this->put('client_id');
        $insert = $this->db->update('ip_clients', $data);

        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_delete() {
        $this->db->where('client_id', $this->delete('ip_clients'));
        $delete = $this->db->delete('ip_clients');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }




}
