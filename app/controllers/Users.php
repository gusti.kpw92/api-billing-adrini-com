<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Users extends REST_Controller  {

	function __construct($config = 'rest') {
        parent::__construct($config);
    }

	function index_get() {
        $id = $this->get('user_id');
        if ($id == '') {
            $data = $this->db->get('ip_users')->result();
        } else {
            $data = $this->db->where('user_id', $id)
                            ->get('ip_users')->result();
        }
        $this->response($data, 200);
    }

    function index_post() {
        $my_pass = $this->_crypt($this->input->post('user_password'));

        $data = array(
            'user_type' => $this->input->post('user_type'),
            'user_active' => $this->input->post('user_active'),
            'user_date_created' => date('Y-m-d m:s'),
            'user_date_modified' => date('Y-m-d m:s'),
            'user_language' => $this->input->post('user_language'),
            'user_name' => $this->input->post('user_name'),
            'user_company' => $this->input->post('user_company'),
            'user_address_1' => $this->input->post('user_address_1'),
            'user_address_2' => $this->input->post('user_address_2'),
            'user_city' => $this->input->post('user_city'),
            'user_state' => $this->input->post('user_state'),
            'user_zip' => $this->input->post('user_zip'),
            'user_country' => $this->input->post('user_country'),
            'user_phone' => $this->input->post('user_phone'),
            'user_fax' => $this->input->post('user_fax'),
            'user_mobile' => $this->input->post('user_mobile'),
            'user_email' => $this->input->post('user_email'),
            'user_password' => $my_pass['user_password'],
            'user_web' => $this->input->post('user_web'),
            'user_vat_id' => $this->input->post('user_vat_id'),
            'user_tax_code' => $this->input->post('user_tax_code'),
            'user_psalt' => $my_pass['user_psalt'],
            'user_all_clients' => $this->input->post('user_all_clients'),
            'user_passwordreset_token' => $this->input->post('user_passwordreset_token'),
            'user_subscribernumber' => $this->input->post('user_subscribernumber'),
            'user_iban' => $this->input->post('user_iban'),
            'user_gln' => $this->input->post('user_gln'),
            'user_rcc' => $this->input->post('user_rcc'),
            
        );
        // $insert = $this->db->insert('ip_users', $data);
        // if ($insert) {
            $this->response($data, 201);
        // } else {
        //     $this->response(array('status' => 'fail', 502));
        // }
    }

    function index_put() {
        $data = array(
            'user_type' => $this->input->post('user_type'),
            'user_active' => $this->input->post('user_active'),
            'user_date_created' => date('Y-m-d m:s'),
            'user_date_modified' => date('Y-m-d m:s'),
            'user_language' => $this->input->post('user_language'),
            'user_name' => $this->input->post('user_name'),
            'user_company' => $this->input->post('user_company'),
            'user_address_1' => $this->input->post('user_address_1'),
            'user_address_2' => $this->input->post('user_address_2'),
            'user_city' => $this->input->post('user_city'),
            'user_state' => $this->input->post('user_state'),
            'user_zip' => $this->input->post('user_zip'),
            'user_country' => $this->input->post('user_country'),
            'user_phone' => $this->input->post('user_phone'),
            'user_fax' => $this->input->post('user_fax'),
            'user_mobile' => $this->input->post('user_mobile'),
            'user_email' => $this->input->post('user_email'),
            'user_password' => $my_pass['user_password'],
            'user_web' => $this->input->post('user_web'),
            'user_vat_id' => $this->input->post('user_vat_id'),
            'user_tax_code' => $this->input->post('user_tax_code'),
            'user_psalt' => $my_pass['user_psalt'],
            'user_all_clients' => $this->input->post('user_all_clients'),
            'user_passwordreset_token' => $this->input->post('user_passwordreset_token'),
            'user_subscribernumber' => $this->input->post('user_subscribernumber'),
            'user_iban' => $this->input->post('user_iban'),
            'user_gln' => $this->input->post('user_gln'),
            'user_rcc' => $this->input->post('user_rcc')
        );

        $this->db->where('user_id', $this->put('user_id'));
        $insert = $this->db->update('ip_users', $data);

        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function index_delete() {
        $this->db->where('user_id', $this->delete('ip_users'));
        $delete = $this->db->delete('ip_users');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function user_add()
    {
        $my_pass = $this->_crypt($this->input->get('user_password'));

        $data = array(
            'user_type' => $this->input->get('user_type'),
            'user_active' => $this->input->get('user_active'),
            'user_date_created' => date('Y-m-d m:s'),
            'user_date_modified' => date('Y-m-d m:s'),
            'user_language' => $this->input->get('user_language'),
            'user_name' => $this->input->get('user_name'),
            'user_company' => $this->input->get('user_company'),
            'user_address_1' => $this->input->get('user_address_1'),
            'user_country' => $this->input->get('user_country'),
            'user_email' => $this->input->get('user_email'),
            'user_password' => $my_pass['user_password'],
            'user_psalt' => $my_pass['user_psalt'],
        );
        $insert = $this->mdl_ztest->save($data);
        $reply = array(
            'status'    => TRUE,
            'message'   => 'User added succesfully!',
            'data'      => $insert,
        );
        echo json_encode($reply);
    }

    private function _crypt($user_password)
    {
        $db_array['user_password'] = $user_password;

        $this->load->library('crypt');
        $user_psalt = $this->crypt->salt();

        $db_array['user_psalt'] = $user_psalt;
        $db_array['user_password'] = $this->crypt->generate_password($db_array['user_password'], $user_psalt);

        return $db_array;
        // echo json_encode($db_array);
    }




}
